# 單元測試的心法
[影片參考](https://www.youtube.com/watch?v=QoR8xBcxuQs&)
## 大綱
* [測試須知](#user-content-測試須知) 
* [測試function 怎麼寫](#user-content-測試function怎麼寫) 
* [什麼是好的測試](#user-content-什麼是好的測試) 
* [應該避免的測試寫法](#user-content-應該避免的測試寫法) 
* [再提升一點](#user-content-再提升一點) 
* [結語](#user-content-結語) 

<h2 id='測試須知'>測試須知</h2>
不是所有的程式都是'可測試'的，有時為了測試須先重構。 

<h2 id='測試function怎麼寫'>測試function怎麼寫</h2>


### 測試function 怎麼命名 
命名要包含三件事  
 -要被測試的方法名稱.  
 -測試的情境(input).  
 -預測行為.  
 ``` python
def test_greet_when_param_is_adam_should_get_hi_adam(self):
	pass
 ```

### 測試function的內容架構 
內容架構要包含三件事  
 -準備  
 Stub,Mock，參數，假資料...  
 -執行  
 執行要被測試的function.   
 -驗證  
 驗證想要驗證的側向. 
  ``` python
def test_greet_getGreetMsg(self):
	# 準備
	name = 'Adam'
	#執行
	result = greeter(name)
	# 驗證
	self.assertEqual(result,'Hi Adam')
	self.assertIsInstance(result,str)
 ```
 ps:驗證與執行要分離，即使簡單
  ``` python
self.assertEqual(greeter(name),'Hi Adam')
 ``` 

<h2 id='什麼是好的測試'>什麼是好的測試</h2>

 好的測試，不浪費大家的時間<br>
 1. 可信賴 
	不可有bug，不可時好時壞<br>
 2. 可維護  
    每次測試都要花很多時間（越來越難改的測試）。<br>
    程式碼改了測試就要跟著改(思考對外開放的方法)。<br>
    對使用者來說a方法與b方法都不重要，最後得到的list才是最<br> 重要的。   
 3. 可讀性  

<h2 id='應該避免的測試寫法'>應該避免的測試寫法</h2>

1. 可讀性低的測試  <br>
2. 帶有邏輯的測試(while，if,for,loop).  <br>
	帶有邏輯會增加程式的複雜度與潛在bug，且通常意味著要<br> 
	驗證很多東西<br>
3. 帶有try catch 的測試<br>
	測試幫我們發現程式錯誤，若用try catch ，就不知道哪裡<br>
	錯了，可用assertRaises檢測程式有沒有如期引發錯誤。
4. 無法重現的測試<br>
	比如:使用亂數當參數想要多試幾種可能<br>
5. 複雜的命名<br>
	通常意味你一次驗證了許多東西<br>
	(ex:test_purchase_name_getGreetMsgAndGerProductList)
6. 儘量不要用setup<br>
	每個測試需要的材料自己準備，材料不會互相影響<br>
	不會影響到看code的順序<br>
	真的要用setup，setup一定要放所有測試都會共用的東西<br>
7. 測試不應該有先後順序<br>
	不要自己排測試的先後順序<br>
	也不要再測試裡呼叫另一個測試<br>
8. 不要共享狀態<br>
9. 測試asset的值不要動態產生<br>
	self.assertEqual(result,'Hi Adam')比<br>
	self.assertEqual(result,f`Hi {name} 好`)
<h2 id='再提升一點'>再提升一點</h2>

1. 同一個測試，用不同的參數測試組合-參數化測試
2. 搭配面對面的code review 勝過100％的覆蓋率

<h2 id="結語">結語</h2>
測試很多的事情也是寫code要注意的事情<br>
可讀性很重要<br>
要以'不浪費別人時間'的想法來寫




