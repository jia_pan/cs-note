# Functional Programming

## 函數式編程涉及到的概念
### 純函示
* 函數式編程基於純函示
 * 純函示是給定的輸入返回相同輸出的函數，例如
``` js
let double  = value =>value*2
```
 ** 反例，依賴外部變數，當外部變數被改變，其輸出結果不可被預測
 ``` js
let factory = 3
let totalNum  = value =>factory*2
console.log(totalNum(3))
//totalNum 函數依賴變數factory
```
 ** 解決方式，將依賴外部變數傳入
  ``` js
let factory = 3
let totalNum  = (value,factory) =>factory*2
console.log(totalNum(3,factory))
```
達到1.可推測;2.可複用，模塊化;3.無副作用 
* 純函數的意義
 * 純函式可以產生可測試的程式碼
  1. 不依賴外部環境計算，不產生副作用，結果可控，可推測，方便測試
  2. 可讀性強，js不管是否是純函式，都會有一個語意化的名稱
  3. 可以組裝成複雜任務的可能性，符合模塊化，概念與SRP
  
## 不可變性
* Object.freeze
不可變數據：純數據. 
freeze，僅可做淺凍結，
``` js
//實作深拷貝
const deepFreeze = (obj)=>{
  Object.freeze(obj)
  for(let i in obj){
    if(typeof obj[i]==='object'){
        deepFreeze(obj[i])
    }
  }
}
```

* 映射



# 高階函數:(Higher-Oreder Function)

以函數作為input 或輸出的函示

## 高階函數作用
### 抽象
 1. 命令式編程:強調如何做
 ```
 let a = [1,2,3]
 for(let i=0 ;i<arr.length; i++){
   console.log(arr[i])
 }
 ```
 
 2. 申明式編程：強調做什麼，把如何做封裝
 ```
 let a =[1,2,3]
 // forEach 複用性
 let forEach = (arr,fn)=>{
   for(let i =0;i<arr.length;i++){
    fn(arr[i])
   }
 }
 forEach(a,(e)=>console.log(e))
 ```
 前期代碼量多，後期可複用性強


### 緩存 & 惰性
```
//需求，讓test 只執行一次
let test =()=>conosle.log('test')


const once = (fn)=>{
let done = false
return ()=>{
 if(!done) {
  fn()
 }else{
 console.log('已經執行過')
 }
 done = true
}

}
const myOnceTest = once(test)
myOnceTest()//test
myOnceTest()//已經執行過
myOnceTest()//已經執行過
```

惰性：函數是在調用時執行


# Curry 化
把多元函式轉換成一元函式
```
//把二元轉換成一元
const sum = (a,b)=>a+b
const curry = (fn)=>(a)=>(b)=>fn(a,b)
const cSum = curry(sum)
console.log(cSum(1)(2))//3
```
實戰用處
```
const curry = (fn)=>(a)=>(b)=>fn(a,b)
const data = [{name:'小張',age:12},{name:'老王',age:23,{name:'阿明',age:8}}]

//找出小張
let result = data.find(item=>item.name ==='小張')
console.log(result)
//curry 化
const getObj = (name,item)=>item.name = name
const fGetobj =curry(getObj)
result = data.find(fGetObj('小張 '))
// 把多元轉換為一元

const curry = (fn)=>{
    return function curredFn(...args){
       if(args.length <fn.length){
            return function(){
                return curred(...args.concat([...arguments]))
            }
       }
       return fn(...args )
    }
}

```

# 組合

```
const afn = a=>a+2

const bfn = b =>b*4

//得出 2 先加2在乘四的結果
let result = bfn(afn(2))

//壞處:此方法依賴數據，能將 ‘先加2在乘四’ 這功能組合起來嗎？

//借助組合函式，順序從右至左
//管道（pie）從右至左 
const compose = (fn1,fn2)=>(value)=>fun1(fn2(value))
cosnt myfn = compose(bfn,afn)
result = myfn(2)


```



  
  
  
  
  
  
  
  
  
  
  
