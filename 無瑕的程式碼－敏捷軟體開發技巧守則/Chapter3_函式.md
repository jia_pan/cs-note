## 簡短
函式的首要準則是簡短，第二項準則要比第一項函式還要簡短。<br>
每行不該有150個字母那麼長，長度也不該多餘20行

### 區塊(Blocks)和縮排(Indenting)

if、else、while及其他敘述應該只有一行。<br>
這也意味函式不該大到包含巢狀結構。<br>
因此函式裡的縮排不該大過一或兩層。<br>
這將使函式更容易閱讀與理解。

---
## 只做一件事情

>函式應該做一件事情。他們應該把這件事<br>做好。而且他們應該只做一件事情。

觀察函示是否做超過「一件事情」的另一種<br>方法，是看你是否能從函式中，提煉出另一個新函式

---

## 每個函式只有一層抽象概念
一個函式擁有混和層次都抽象概念，總令人困惑，<br>

### 由上而下的閱讀程式碼:降低準則

我們希望閱讀程式碼就像由上而下的敘事所謂**降層準則**，換句話說，在閱讀<br>
程式碼就像一連串的to段落，每個段落敘述目前所處的抽象層次，並提及接續下<br>的層次，並提及接續的下個層次的to段落。

>為了(to)要包含設定和拆解，我們先要納入設定，再納入測試頁的內容，最後納入拆解。<br>
為了要納入這些設定值，若是套件，我們會納入套件設定步驟，然後再引入一般的設定步驟<br>
為了要納入套件設定，我們先搜尋「SuiteSetup」頁面的上層，然後加入納入該頁面路徑的敘述。<br>
為了要搜尋上層....



## Switch 敘述

要讓switch簡短是很困難的事情。就算只有一個switch只有兩種情況。<br>
本質來說，switch總在做N件事情，無法避免使用，但能使敘述被深埋在<br>
抽象層級較低的類別裡，且永遠不會被重複使用。我們可利用多型(Polymorphism)

```Java
public Money calculatePay(Employee e)
throws InvalidEmployeeType{
    switch(e.type){
        case COMMISIONEND:
            return calcualteCommissionedPay(e);
        case HOURLY:
            return calculateHourlyPay(e);
        case SALARIED:
            return calculateSalariedPay(e);
        default:
            throw new InvalidEmployeeTylpe(e.type);
    }
}

```

這個函式有些問題
1.太長，加入新職員型態這個函式將變得更長<br>
2.很明顯的這個函是座超過一件事情<br>
3.因為有超過一個裡有來改變此函式，因此違反SRP<br>
4.當加入新型態，函式必須改變，違反OCP<br>

這個問題的解決方法，將switch 深埋在抽象工廠裡面。

---

## 使用具描述能力的名稱

別害怕去取較長的名稱，一個較常但據描述性質的名稱，比一個較短且難以理解的名稱更好。

---


## 函式的參數

函式參數最理想的參數是0、其次1、其次2，最好保持在3個內，超過3個必須要有特殊理由，<br>
---否則無論如何不該出現。

以測試角度，沒參數的函式也更利於測試。

輸出型參數筆輸入型參數更易於理解。

myNote:

```javascript

function copyAtoB(a/*輸入型參數*/,b/*輸出型參數*/){
    b = a
}



```

## 單一參數的常見型式
常見理由
1.與這參數有關的問題
```java
boolean fileExists("MyFile");
```
2.對這個參數進行某種轉換
```java
InputStream fileOpen("MyFile");
```

以上兩者讀者看到都能有預期結果。
還有一種常見的單一參數類型，就是**事件(event)**<br>
例如代表密碼輸入失敗次數的void passwordAttempFailedNtimes(int attempts)函式，<br>
小心使用，並讓讀者清楚了解這是一個事件。

若不符合上面型式，試著不要使用單一參數。
再轉換時，使用輸出型參數而不使用返回值令人困惑。

---


## 旗標(flag)參數
使用旗標參數是一種很爛的做法，將一個布林傳你給函式是一種恐怖習慣。
等於大聲宣布這個函式做了不只一件事情。
方法呼叫render(true)，讀者會困惑，移到方法render(isSuite)或許有幫助，<br>
最好的方式應該將此函式分成renderForSuit()和renderForSingleTest()。

---

## 兩個參數的函式

一個參數好過兩個參數，若有機制可以轉換，你就該好好利用，例如<br>

writeField(outputStream,name)變為writeField(name)。

---

## 三個參數的函式

理解三個參數的函式比理解兩個參數的函式更為艱難，應該謹慎思考。



### 物件型態的參數

一個函式的參數超過三個，就必須利用物件來傳遞。



### 參數串列
有時我們希望能傳遞不同數量的參數給函式，參數可以是1、2、3，超過就是錯誤了。


### 動詞與關鍵詞
替函數選個好名稱，可以產生許多好的附加價值，如解釋函式意圖、解釋函式參數順序性。

writteFild(name)好過 write(name)，
assertExpectedEqualsActual(expected,actual)好過 assertEquals。

---

## 要無副作用

你的程式保證只做一件事情，卻**暗地**偷偷做了其他事情。<br>

有時會使同類別其他變數產生不可預期的改變<br>

有時他會將之轉圜成參數傳遞給其他函式，或轉變系統的全域變數。<br>

這兩種都是詐欺與有害的不信任行為，常導致奇怪的時空耦合(temporal couplingg)<br>
與順序性問題。

### 輸出型參數

參數大多情況都被解讀成輸入型，例如
```java

appendFooter(s);
```
這個函式真的是把s接在某東西後面，還是把某東西接在s後面?<br>
你必須去看函式如何宣告，才能弄清楚。這弄斷了我們的思考，要避免。

物件導向設計尚未出現，有時需要輸出型參數。<br>
整體而言，應該要避免使用輸出型參數。若你的函式必須要改變物件狀態，<br>
就該讓該物件改變自身的狀態。

---

## 指令與查詢分離
函式要能做某件事情，或回答某個問題，但兩者不該同時發生，同時發生會讓人困惑，<br>
如以下函式例子
```java

public boolean set(String attribute,String value);
```

這函式設定某屬性值，成功回傳true，反之false。這導致下面詭異敘述:
```java

if(set("username","unclebob"))...
```

這代表什麼?
1.username已經被設定為unclebob?
2.username設定為unclebob是否成功?

我們可以把set重名為setAndCheckInExists來增加if可閱讀性，但無太大幫助。<br>
最好作法將指令(command)與查詢(query)分開，才能避免模稜兩可的情況。

```java

if(attributeExists("username")){
    setAttribute("username","unclebob");
}
```

---

## 使用例外處理取代回傳錯誤碼



## 提取Try/Catch區塊
指令型函式回傳錯誤碼，有點違反命令查詢分離原則，代表鼓勵在if判別  
將指令型函數當作判斷表達式使用。
```java

if(deletePage(page)=E_OK)
```

這樣作法不會引起動詞/形容詞困惑，卻會造成更深的巢狀結構。  
你回傳錯誤碼，就是要求呼叫者馬上處理這個錯誤。

```java

if(deletePage(page)=E_OK){
    if(registry.deleteReference(page.name)==E_OK){
        if(configKeys.deleteKey(page.name.makeKey())==E_OK){
            logg.log("page delete");
        }esle{
            logger.log("configKey not delete");
        }
    }else{
        logger.log("deleteReference from registry faild");
    }
}else{
    logger.log("delete faild");
    return E_ERROR;
}
```
若使用例外處理取代錯誤帶碼，那錯誤處理成式碼可以抽出來簡化代碼。

```java

try{
    deletePage(page);
    registry.deleteReference(page.name);
    configKeys.deleteKey(page.name.makeKey());
}catch(Exception e){
    logger.log(e.getMessage());
}
```

### 提取Try/Catch區塊

Try/Catch本身難看，所以好的做法式將之抽離出來。
### 錯誤處理就是一件事情
函式指該做一件事情，而錯誤處理就是一件事情，一個錯誤處理的函式就不該  
在做其他事情，代表try就是開頭，catch/finally區塊之後就不該有程式碼。

---

## 不要重複你自己
重複程式碼是許多軟體裡所有邪惡的根源。許多規則或慣例都是匯了控制或移除他。  
如資料庫的Codd's normal forms(柯德正規法)、物件導向程式設計。結構化程式設計、  
剖面導向程式設計、元件導向程式設計等等

---

## 結構化程式設計

每個函式、與每個函式裡的區塊都只有一個進入點與一個離開點。要遵守這個守則代表  
只能有一個return，迴圈內不能有任何break或contiune，且永遠不可以有goto  
這些準則在小函數中助益有限，只有在大型函數才能表現出巨大好處。  
若你能保持函數短小，那麼偶而出現retrun、break或continue沒有壞處。




