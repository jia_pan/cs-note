## 註解無法彌補糟糕的程式碼
與其花時間寫註解來解決你造成的混亂，不如花時間整理混亂的程式碼

## 用程式碼表達你的本意

``` java

if((employee.flags & HOURLY_FLAG)&&
   (employee.age)>5
)

```
``` java

if(employee.isEligibleForFullBenfiets())

```
經由以上兩種，你希望看到哪種程式碼?

## 有益的註解

真正有益的註解，是你想辦法不寫它的註解。

### 法律型註解
```java
// Cpyright (C) 2003,2004,2005 by Object Mentor, Inc .All rights later.
// Released under the terms of the GUn General Public License version 2 or later

```

有時公司的程式碼標準規範強迫我們寫下某些法律行註解。  
內容不該直接是契約或法律條款。如果可以，讓註解去參考  
一個標準的許可或其他的外部文件會比直接寫在註解裡面還好。

### 資訊型註解
透過註解提供一些基本資訊室非常有用的。例如下面註解說明了一個抽象方法  
的回傳值
```java
// Returns an instance of the Responder begin tested.
protected abstract Responder responderInstance()
```
這樣的註解是有意義的，如果可以用函式名稱傳達訊息會更好。如將函示重新  
命名 responderBeingTested ，那此例的註解就變成多餘了。

### 對意圖的解釋
註解不僅提供關於時做有用的資訊，也提供某個決定背後的意圖資訊。
```java
public int compareTo(Object o)
{
  if(o instanceof WikiPagePath)
  {
   WikiPagePath p = (WikiPagePath)o
   String comparessdName = StringUtil.join(names,"");
   String comparessdArgumentName = StringUtil.join(p.names,"");
   retutn comparessdName.cmpareTo(comparessdArgumentName);
  }
  return 1;//we are greater because we are the right type
}

```

### 闡明 
有時把難解的參數或回傳值翻譯成具可讀性的文字，這種註解是有幫助的。

```java
public void testComparTo() throws Exception
{
.....
assertTrue(a.compareTo(a)==0) //a == a
assertTrue(a.compareTO(b)!=0) //a != b
  
}
```

## 對於後果的告誡
有時能警告其他設計師，會出現某種特殊後果的註解，也是有用的。

```java
// Don't run unless you
// have some time to kill
public void _testWithReallyBigFile()
{
....

}

```

### TODO(待辦事項)註解
以//TODO 形式留下一些「待辦」的筆記在註解裡面是合理的行為。


### 放大重要性
某些無關緊要的事情，可以透過註解來放大其重要性。
```java
String listContent = math.group(3).trim();
//the trim is real important. It removes the starting
//spaces that could cause the item to be recognized
//as another list.
new ListItemWidget(this,listItemContent,this.level+1);
return buildList(text.substring(match.end()));

```

## 糟糕的註解

### 喃喃自語
若你要寫下一個註解，應該要花上必要的時間確保你寫出的是最好的註解。
```java
String listContent = math.group(3).trim();
public void loadProperites()
{
  try
  {
    String propertiesPath = propertiesLocation+"/"+PROPERTIES_FILE;
    FileInputStream propertirsStream = new FileInputStream(propertiesPath);
    loadedProperties.load(propertiesStream);
  }
  catch(IOEception e)
  {
    //No properties files mean all default are load
  }
}

```
這段註解代表甚麼?只能明顯看出，若執行出現了 IOEception，代表找不到屬性檔案，所有  
預設值都會被載入。但何時被載入?loadedProperties.load之前，還是  
loadedProperties.load 捕捉到例外後?還是 loadedProperties.load 在嘗試載入檔案前?  
我們唯一的辦法只有檢察系統其他部分才能得知。這是種失敗註解，不值得我們保留。


### 多餘的註解
若註解沒比程式碼透露多訊息，意圖或理由，甚至更難理解，多是多餘的註解。

### 規定型註解
若有條規定是某個函式都必須有個javadoc，或每個變數都應該要有註解說明，這樣規定夠傻  
這樣只會讓程式碼更加凌亂，傳遞更多謊言，產生更多困惑和製造散亂的程式結構。

### 日誌型註解
以前沒有原始碼管控系統，現在沒有存在的理由了。

### 干擾型註解
沒有用處的註解只會干擾我們，僅陳述明顯的事實。


###  當你可以使用函式或變數時就不要使用註解

```java
// dose the module from the global list<mod> depend on the
// substem we are part of ?
if(smodule.getDependSubsystem().contains(subsysMod.getSubstem()))

```

可被改寫成沒有註解的版本

```java
// dose the module from the global list<mod> depend on the
ArryList moduleDependees = smodule.getDependSubsystems();
String ourSubsystem = subSysMod.getSubSystem();
if(moduleDependees.contains(ourSubsystem))

```

### 位置的標誌物


```java
// actions//////////////////////////////////

```
只有在極小特殊情形下，才會利用這種橫幅來聚集某些特定韓式，但通常只是一種凌亂物。

### 又大括號後面的註解
```java
try{
.....
  while(){
  
  }//while
}//try
catch(){

}//catch

```

這對較長且深層巢狀結構的函式可能是有意義的，對於短小封裝的我們而言，只會凌亂。

### 出處及署名
```java
/* Added by Rick*/

```
有了版控，我們再也不需要。

### 被註解的程式碼。
你有版控了，你該刪除掉，當別人看到被註解的程式碼也無勇氣刪除。

```java
///WikiPagePath p = (WikiPagePath)o
///String comparessdName = StringUtil.join(names,"");
///String comparessdArgumentName = StringUtil.join(p.names,"");
///retutn comparessdName.cmpareTo(comparessdArgumentName);
```












