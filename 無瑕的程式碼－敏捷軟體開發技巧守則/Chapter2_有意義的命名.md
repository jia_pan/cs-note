## 總結
要挑選一個好的名稱，最困難的點在於需要良好的描述技巧與共通文化背景<br>

---
## 讓名稱代表意圖 --使之名副其實

當發現有更好的名稱，應該替換掉原本的。<br>
變數、函式會類別名稱要能解答大多問題，若一個名稱還須<br>
註解解釋，那麼這個名稱不具備展現意圖的能力

```java
int d ;//elapsed time in days

```
名稱d沒有表達任何資訊，我們應當命名具體

```java
int eleaspedTimeInDays;
int daysSinceCreation;
int daysSinceModification;
int fileAgeInDays;

```

好的意圖名稱，更容易了解程式碼，下列程式碼說明了甚麼?
```java
public List<int[]> getThem(){
	List<int[]>list1 = new ArrayList<int[]>();
	for(int[]x:theList)
		if(x[0]==4)
			list.add(x);
	return list1;

}
```

1. theList 裡面放甚麼類型的東西
2. theList索引0的項目代表什麼
3. 數值4的意義是什麼?
4. 我該如何使用回傳列表

```java
public List<Cell> getFlaggedCells(){
	List<Cell>flaggedCells = new ArrayList<Cell>();
	for(Cell cell :gameBoard)
		if(cell.isFlagged())
			flaggedCells.add(cell);
	return flaggedCells;
}
```
---

## 避免誤導
我們該避免使用那些與原意相違背的單字，如hp、aix、sco因為他們都是Unix平台或其他平台的名稱<br>
不要用accountList代表一群帳戶的變數名稱，出非該變數的型態真的是List，可使用accountGroup、<br>
bunchOfAccounts或簡單的accounts。
<br>
名稱若只有一些不同，使用時要小心，XYZControllerForEfficeientHandleOfStrings與<br>
XZYControllerForEfficientStoreageOfStrings，這兩個看起來太過相似了。

---
## 產生有意義的區別
```java
public static void copyChars(char a1[],char a2[]){
	for(int i=0;i<al.length;i++){
		a2[i]=a1[i]	
	}
}
```

若改用source與destination當作參數名稱，將更容易猜測其意圖。<br>
無意義的命名也是多餘的，varable這個字眼不該出現在變數名稱裡，<br>
table這個字眼也不該出現在表格名稱裡

---

## 使用能唸出來的名稱
不要使用簡寫，比如genymdms，意思是產生日期(generation data )、年(year)，月(month)、天(day)<br>、小時(hour)、分鐘(miunte)、秒(seconds)。

---
## 使用可被搜尋的名字
若一個變數或常數在程式不少地方都用的到，那最好給他們一個容易搜尋到的名稱。<br>
WORK_DAYS_PER_WEEK，比起搜尋5要容易搜尋的到。

---

## 避免編碼
編碼已夠多，不要將將型態話視野資訊放到名稱裡面。

X m_dsc(代表string)
X IShapFactory(形狀介面)
---

## 避免思維轉換
在迴圈裡記數變數設為單個字母ijk是慣例，但其餘部分不該將變數設為單字母，「清楚明白才是王道」
---

## 類別的命名
類別與物件應以名詞或動名詞命名，不該是動詞。

---

## 方法命名
方法應以動詞或動詞片語命名
---

## 不要裝可愛
如果命名只有懂作者幽默感的人才會記得這些名稱，這是不太好的，例如HolyHadnGrenda(神聖手榴彈)

---
## 每個概念使用一個字詞
在命名不同類別的取得方法，採用fetch、retrieve和get這些不同名稱，這樣會困擾我們。

---

## 別說雙關語
避免使用一個字詞代表兩種目的。

---

## 使用解決方案領域的命名
儘量以電腦科學領域的術語，如演算法名稱、模型名稱、數學詞彙等來明明。

---
## 使用問題領域的命名
若沒有程式設計師熟悉的術語，請使用該問題領域的術語來命名。

---

## 添加有意義的上下文資訊(Context)
state是甚麼意思，若跟隨著city、zipcode ，你會知道是代表州，可以利用自首增加上下文資訊<br>
addrFirstName、addrState(地址的州)等等。

---

## 別添加無理由的上下文資訊
你有一個虛構的應用程式，叫「豪華版的加油站(Gas Statrion Deluxe)」，因此應用中，所有<br>
類別都加上GSD字首，這聽起來不是好主意。
較小的名稱若能能清楚表達通常好過較長的名稱。
