# keeep alive
[影片]('https://youtu.be/KxtdOSFWL6M?t=1438')<br>
v-if 切換組件希望保持狀態，可使用keep-alive 組件

```vue

<keep-alive>

    <component :is="currentTab"
                name:'123'/>
</keep-alive>

```

## 屬性
* include -string|RegExp|Array。僅名稱(name)匹配組件被緩存
* exclude -string|RgExp|Array 。 任何名稱被匹配的組件都不會被緩存。
* max     -number|string      。 最多可緩存多少組建實例，達到這個數字<br>，緩存組件中最近沒被訪問的實例將被銷毀


# 異步組件

組件一多，首屏加載會很慢

## 關於webpack 打包
webpack 打包，會將子己撰寫的程式與引用的第三方的程式編譯為app.js 與<br> chunk-vendors兩隻，而webpakc 所打包的依賴依據是個組件的import 部分，<br>
形成依賴樹，可以利用import 函數，將自己寫的函數，變成另外一隻檔案
```ts

//import {sum} form './math'
// console.log(sum(2,3))


//通過import 函數導入模塊，後續webpack會對其打包時進行分包動作
import('./math').then(res => console.log(res.sum(2,3)))

```

此時打包資料夾會多一隻 chunk.js

## Vue 中實現異步組件
若項目太大，對於某些組件我們希望透過異步方式加載(目的在於進行分包處理)<br>
那vue. 提供了defineAsyncComponent 函式
defineAsyncComponentru將受兩種類型參數：<br>
* 類型一：工廠函數，該工廠需要返回Promise物件
* 類型二：接受一個物件，對異步函數進行配置
異步組建命名以Async為開頭命名

import Loading from './Loading.vue' 
```
//調用處
import {defineAsyncComponent} from 'vue'
const AsyncCategory = defineAsyncComponent(()=> import('./AsyncCategory.vue'))

//or
const AsyncCategory = defineAsyncComponent( {
loader:()=>import('./AsyncCategory.vue'),
//佔位組件，還沒加載近來，要顯示的組件
loadingComponent:Loading,
//errorComponent,
//延遲
delay:2000
})

```

## Suspense
全局組件，該組件兩插槽

* default: 若default可顯示，就顯示default內容
* fallback: 若default無法顯示，就顯示fallback 內容
```vue


<suspense>
    <template #default>
        <async-categrory/>
    </template>
    <template #fallback>
        <loading/>
    </template>

</suspense>

```

