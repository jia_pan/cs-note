# 為什麼需要babel
[參考](https://www.youtube.com/watch?v=uxZrnmt_xNo)

* 使用es6語法，ts，開發react 都離不開babel，所以學習babel對於我們理解代碼從編寫到上線的轉變過程至關重要。

* 什麼是babel 
  * babel 是一個工具鏈，主要用於較遊覽器或環境將es5以上的代碼轉換為兼容的js，包含語法轉換，源代碼轉換等。  
  * 本生可以獨立使用，不和webpack等構建工具配置單獨使用。
  * 若在cmd要使用，須按裝
  ```
  npm install @babel/core babel/cli -D
  ```
  
* 使用babel
  ```
  npx babel demo.js --out-dir dist
  ```
  編譯 demo.js文件，並導到 dist 資料夾

  * 若想把某些語法進行轉換，就要安裝相關插件，如將箭頭函示語法轉換
  ```
  npm install @babel/plugin-transform-arrow-functions -D
  ```

  執行babel
  ```
  npx babel demo.js --out-file test.js --plugins=@babel/plugin-transform-arrow-functions
  ```

  babel有提供預設插件集
  ```
  npm install @babel/present-env -D
  ```

  ```
  npx babel demo.js --out-file test.js --presets = @babel/preset-env
  ```
