# Webpack 基本使用
[影片]("https://www.youtube.com/watch?v=A3ChZX81PmA&t=3540s")

## install

* webpack 安裝分為兩個 webpack 、webpack cli 
  * 執行webpack命令，會執行node_module 下 .bin 目錄下的webpack
  * webpack 在執行時依賴webpack-cli，沒安裝會報錯
  * webpack-cli中代碼執行時，才是真正利用webpack進行編譯與打包的過程
  * 安裝webpack 時，我們需要同時安裝webpack-cli


  全局安裝（一般較少使用）

    ```
	  npm install webpack webpack-cli -g
	```
  局部安裝
  要先有pack.json

    ```
	  npm init
	```    
	在安裝webpack
	```
	  npm install webpack webpack-cli -D
	```
	-D 為 --save-d 簡寫，代表安裝為開發依賴

	此時在cmd 執行 webpakc ，依然為調用全局webpack，想要調用局部，將package.json檔案中的script 寫入
	```
	  “script":{
	  	"build":"webpack"
	  }
	```	
	此時會優先調用局部webpack，node_modules->.bin->webpack
## 打包原理

webpack 打包會默認找尋 src->index.js 進行打包，打包到dist資料夾   
想要改變.  
*方法1 命令行
```
 npx webpack --entry ./src/main.js -output-path ./build
```	
npx 指令會優先檢查本地，本地找不到才會去找全局。
entry 代表入口，ourput-path 代表輸出

*方法2 配置webpack.config.js
```js
const path = require('path')
module.exports = {
  entry: './src/main.js',
  output: {
    //path 必須為絕對路徑
    //因此使用path 幫我們獲取絕對路徑，__dirname 代表獲取當下文件路徑
    path: path.resolve(__dirname, './build'),
    //輸出文件名稱
    filename: 'bundle.js',
  },
}
```

## webpack 依賴圖

webpack 會從入口開始，查找所有依賴，沒有依賴的不會打包成靜態資源

## webpack 引入css
webpack 本身不支持引入css，因此需下載css-loader，並進行配置
```js
const path = require('path')
module.exports = {
  entry: './src/main.js',
  output: {
    ...
  },
  module:{
  	rules:[
  		test:/\.css$/,//規則test，找到css 檔案後
  		// 1.loader 的寫法(語法糖)
  		//loader:'css-loader'//使用css-loader

  		// 2.完整寫法
  		use:[
  			//{loader:'css-loader'}
  			"css-loader"
  		]
  	]
  }
}
```
我們可以透過css-loader加載css 文件了，但會發現這個css中沒有生效  
因為 css-loader 僅負責.css文件解析，並不會將解析完的css插入頁面，  
若希望插入頁面，需要另一個loader，style-loader. 

安裝style-loading
```
npm install style-loader -D
```

配置

```js
const path = require('path')
module.exports = {
   ...,

  module:{
  	rules:[
  		test:/\.css$/,//規則test，找到css 檔案後
  		// 1.loader 的寫法(語法糖)
  		//loader:'css-loader'//使用css-loader

  		// 2.完整寫法
  		use:[
  			//{loader:'css-loader'}
  			"style-loder",
  			"css-loader"

  		]
  	]
  }
}
```
注意loader順序，loader 是從後向前。
