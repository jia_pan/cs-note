1.注入器

constructor(private productService:ProductService){}//申明需要ProductService(這是在聲明，我要ProductService這個token)


2.提供器//聲明在module 中，可以申明在appMoudule的providers，也可以聲明在組件的providers

providers:[ProductService]
//等同於
providers:[{provide:ProductService,useClass:ProductService}]
//有人要ProductService這個token，使用 new ProductService 實例化給他用

也可以用工廠方法返回

providers:[{provide:ProductService,useFactory:()=>{...})}]


提供器規則
1.若聲明在module中，他是對所有組件的可用的


服務類申明上，有個@Injectable()

他代表可以把其他服務，注入當前的服務