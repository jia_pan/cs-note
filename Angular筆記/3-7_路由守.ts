路由守衛

	*當用戶只有權限才能進入某路由


	*一個多頁表單組成的導向，如註冊流程，只有當用戶填完當前才能跳到下一步的路由


	*當用戶沒有存檔，要跳到其他頁面時要通知用戶




路由守衛

CanActivate:處理導航到某路油的情況


CanDeactivate:處理當前路游離開的情況


Resolve:在路由基活之前獲取路由數據


//範例 登入驗證 CanActivate

1.新增一個登陸檢查類//login.guard.ts

import {CanActivate} from "@angular/router"
export LoginGuard implements CanActivate{
	cansActivate(){
		let loggedIn:boolen = 檢查登入

		if(!loggedIn){
			console.log("尚未登入")
		}
		return loggedIn;
	}

}

//app-routing-module.ts

import{LoginGuard} from "./guard/login.guard"

const rotes:Routes = [
	{path:"",redirectTo:'/home',pathMatch:'full'},//redirectTo 重定向
	{path:"index",component:"IndexComponent"},
	{path:"/product/:id",component:"ProductComponent",
	children:[
		{path:'',component:ProductDescComponent},
		{path:'seller/:id',component:ProductDescComponent},
	],cansActivate:[LoginGuard]
	},//
	{path:"**",component:"Code404Component"}]

	@NgModule({

		...省略

		providers:[LoginGuard]//添加依賴注入
	})




//範例 離開檢查 CanDeactivate
1.新增一個登陸檢查類//unsave.guard.ts

import {CanDeactivate} from "@angular/router"
export UnsaveGuard implements CanDeactivate<要保護的組件>{
	CanDeactivate(component:要保護的組件){
		let loggedIn:boolen = 檢查登入

		return window.confirm("你還沒保存，確定要離開嗎")
	}

}


//app-routing-module.ts

import{LoginGuard} from "./guard/login.guard"
import{UnsaveGuard} from "./guard/unsave.guard"

const rotes:Routes = [
	{path:"",redirectTo:'/home',pathMatch:'full'},//redirectTo 重定向
	{path:"index",component:"IndexComponent"},
	{path:"/product/:id",component:"ProductComponent",
	children:[
		{path:'',component:ProductDescComponent},
		{path:'seller/:id',component:ProductDescComponent},
	],cansActivate:[LoginGuard],
		CanDeactivate:[UnsaveGuard]
	},//
	{path:"**",component:"Code404Component"}]

	@NgModule({

		...省略

		providers:[LoginGuard,UnsaveGuard]//添加依賴注入
	})
