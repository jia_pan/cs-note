<img [src]="imgUrl">

<img={{imgUrl}}>

等值，統一就好

HTML屬性vsDOM屬性

HTML屬性經過初始化後，值是不變的

<input value="Tom" (input)="doChange($event)">



//
doChange(event:any){

	console.log(event.target.value);//DOM屬性

	console.log(event.target.getAttribute('value'));//HTML屬性

}
//補充 button 的disable加上去就會禁用，儘管設為daiable='false'
//因此要改，要利用DOM屬性去改

HTML屬性與DOM屬性的關係

1.少量HTML屬性和DOM屬性有映射關係，如id

2.以些HTML屬性沒有對應DOM屬性，如colspan

3.有些DOM屬性沒有對應HTML屬性，如textContent

4.就算名字相同，HTML屬性與DOM屬性也不是一樣的東西

5.HTML屬性值指定了初始值;DOM屬性表示當前值
DOM屬性可改變，HTML的屬性初始化後不能改變

模板綁定是透過DOM屬性與事件來工作，而非HTML屬性


