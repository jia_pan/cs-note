學習內容
1.了解路由基礎
2.了解子路由、保護路由、輔助路由
3.項目添加路由

//給現有ng 安裝 router
ng new rooter --routing



//路由基本物件

Routes					路由配置，保存哪個URL對應哪個component，以及在哪個RouterOutlet中展示


RouterOutlet 		在html中標示路由呈現內容位置的占位符指令

Router(js中) 		負責載運行時執行路由方法的物件，可通過調用 navigater()和navagateByUrl()方法導航到指定路由


RouterLink(HTML)在HTML終生名導遊航路由用的指令


ActivatedRoute 	當前激活的對象路由，保存著當前訊息，ex路由地址，路由參數等等


//app-routering-module


//Routes
const rotes:Routes = [
	{path:'index',component:'IndexComponent'},
	{path:'/index',component:'IndexComponent'},//這是錯的，path 不用加/
	{path:'**',component:'Code404Component'},//通配符，找不到顯示Code404Component
]

//
`
<a [routerLink]="['/']">主頁</a>
<a [routerLink]="['/poduct']">商品詳情</a>
<button (click)="toDeatalPage()">取得頁面</button>
<router-outlet></router-outlet>
`


toDeatalPage()
{

	this.router.navigater(['/poduct'])	
}