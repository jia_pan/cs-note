//https://www.youtube.com/watch?v=bDyZTsTJIhk&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=38

//本章重點:利用emit將事件打出去




//ChildComponent.ts
@Component({
	//略...	
})
export class ChildComponent{



	@Output();//()參數裡面可以寫自訂名稱，名稱就是外面捕獲該用的名稱，ex:@Output('XXX')，<app-child (XXX)="...."
	emitData:EmitEmitter<stirng>= new EmitEmitter();//EmitEmitter 要用angular/ core 的
													//泛型<>，裡面包的就是你要emit出去的資料類型


	doEmit(){

		this.emitData.emit("emit的資料");		
	}

}
//FatherComponent.html


<app-child (emitData)="emitDataHandler($event)"></app-chile>

//FatherComponent.ts

export class FatherComponent{

	emitData:string = "";


	emitDataHandler(event:string//你發射的類型是甚麼，這邊就是甚麼類型){
		this.emitData = event;
		
	}

}