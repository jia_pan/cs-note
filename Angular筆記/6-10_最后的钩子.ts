//https://www.youtube.com/watch?v=ABLQWrME5mg&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=45



ng 整個生命週期


組件初始化階段
constructor//實例化對象

ngOnChanges//初始化輸入屬性(input


ngOnInit//初始化除了輸入屬性外的所有屬性


ngDoCheck//執行變更檢查

//上半部完成後，開始準備渲染畫面

ngAfterContentInit

ngAfterContentChecked

//上面是渲染組件投影內容

ngAfterViewInit

ngAfterViewChecked

//剩下的是完成整個組件完整的渲染


檢測變化階段
ngOnChange

ngDoCheck

ngAfterContentChecked

ngAfterViewChecked


組件銷毀階段段

ngOnDestroy

/////

ngAfterViewInit 與 ngAfterContentInit 差別在於

ngAfterContentInit 是組件'被投影'組裝完觸發

ngAfterViewInit 是'整個'組件都被組裝完觸發，(所以在這階段改變組件內容執會報




