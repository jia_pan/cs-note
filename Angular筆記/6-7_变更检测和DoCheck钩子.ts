//https://www.youtube.com/watch?v=REq6xomdpis&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=42
AN的變更檢測，是由zone.js實現


變更檢測



Default策略

父組件(產生變化)
	|
	|
	V
子組件(捕獲)
	|
	|
	V

孫子組件(捕獲)




OnPush策略

父組件(產生變化)
	|
	|
	V
OnPush
子組件(停止捕獲)
	|
	|
	V

孫子組件



DoCheck 會檢測任何變化(就連鼠標點input 都會觸發)
所以docheck 要寫得很小心，不然容易引起效能問題 