//https://www.youtube.com/watch?v=oYmTjuJvF0s&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=37
//本章重點:**1.將值由上層(父)傳輸到下層(子)，資料綁定是為單向資料流



//一、基本的值綁定


//fatherComponent.html

<parnetComponent [dataValue]="dataValue" ></parnetComponent>

//FatherComponent.ts

@Component({
	//略...	
})
export class FatherComponent{

	dataValue="父組件資料";


}

//ChildComponent.ts
@Component({
	//略...	
})
export class ChildComponent{

	@Input()	//重點，申明值是由外層賦予，**2.沒有寫這個妝飾器，沒有辦法在父組件寫//[dataValue] 的
	dataValue:sting;


}



//綁定路由的參數


@Component({
	//略...	
})
export class XXXComponent{

	@Input()	//
	dataValue:sting;

	constructor(routerInfo:ActivatedRoute){//重點，依賴注入路由


	}


}