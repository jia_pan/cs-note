為啥有DOM屬性綁定，還要用HTML屬性綁定

1.因為有些屬性就是只有HTML屬性，像colspan
2.class 只有HTML屬性能綁
3.style 只有HTML屬性能綁
 

<table>
		<tr><td colspan={{1+1}}></td></tr>
</table>

//報錯，td 沒有colsapn的DOM屬性
//前墜attr
<table>
		<tr><td [attr.colspan]={{1+1}}></td></tr>
</table>


//class 綁定，前墜class
//方法一 存字串
[class]="myClass"

myClass="btn btn_success"


//方法二動態
[class]="myClass" [class.c]="isBig"

.c{
		font-size:500px

}


//方法三 物件型態

[ngClass]="myClass"


myClass={
		a:false,
		b:ture,
		c:false

}


