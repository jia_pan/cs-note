//https://www.youtube.com/watch?v=ZD_8wtJrEjs&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=5
啟動 Angular 應用
1.啟動時加載了哪個頁面
2.啟動時加載了哪個腳本
3.這個腳本幹了啥事


main.ts 負責引導an的啟動


//mina.ts

import { enableProdMode/*這個方法用來關閉an的開發者模式*/ } from '@angular/core';
import { platformBrowserDynamic /*這個方法告訴an用哪個模塊來啟動*/ } from '@angular/platform-browser-dynamic';

import { environment/*環境配置*/ } from './environments/environment';
import { AppModule/*整個應用的主模塊*/ } from './app/app.module';


if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)//
  .catch(err => console.error(err));
//1.利用platformBrowserDynamic 啟動應用，然後加載AppModule，並分析需要哪些模塊(AppModule中的import中的項目)
//並分析這些模塊需要那些模塊，並執行加載

//2.在index.html中尋找啟動模塊(AppModule)指定的祖組件(bootstrap選項所指定的AppComponent)中的selector選擇器(就是app-root)
//並利用其template所指定模板內容 替換 index.html 的 app-root標籤，在這過程中畫面會先顯示 <app-root> 內的內容