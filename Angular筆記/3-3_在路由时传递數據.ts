在路由中傳遞
1.在路由查詢參數中傳遞數據
/product?id=1&name=2   =>  AcrivatedRoute.queryParams[id]


2.在路由路徑(URL)中傳遞數據

路由定義										URL								取值
{path:"product/:id"}		=> /product/1		=>	 AcrivatedRoute.params[id]

3.在路由配置中傳遞數據


{path:"product",component:ProductComponent,data:[{isProd:true}]}
=>	ActivatedRoute.data[0][isProd]


//1.在查詢參數中傳遞數據
`
<a [routerLink]="["/"]">主頁</a>
<a [routerLink]="["/poduct"]"
	 [queryParams]="{id=1}"// 倒到新頁，url會自動帶上 ?id=1
>商品詳情</a>
<button (click)="toDeatalPage()">取得頁面</button>
<router-outlet></router-outlet>
`

//JS中取得

constructor(private routeInfo:ActivatedRoute){}

ngOnInit(){
	this.productId = this.routeInfo.snapshot.queryParams["id"]

}

//2.在路由路徑中傳遞數據

1.修改配置，使路由可以帶參數

const rotes:Routes = [
	{path:"index",component:"IndexComponent"},
	{path:"/product/:id",component:"IndexComponent"},//
	{path:"**",component:"Code404Component"}]

2.修改連接路由參數
`
<a [routerLink]="["/"]">主頁</a>
<a [routerLink]="["/poduct",1]"//這個array第二個數字變成傳遞的參數
>商品詳情</a>
<button (click)="toDeatalPage()">取得頁面</button>
<router-outlet></router-outlet>
`
3.取得參數

constructor(private routeInfo:ActivatedRoute){}

ngOnInit(){
	this.productId = this.routeInfo.snapshot.params["id"]//queryParams改成params

}

二、參數訂閱


ngOnInit(){
	this.productId = this.routeInfo.snapshot.params["id"]//queryParams改成params

}
//這個方式只有在init 會調用，若同一個組件頁，但ID不同時換頁，裏頭的資料不會被替換(因為都是同一個組件頁)
//為了解決這個問題，可以利用訂閱方式


ngOnInit(){

	this.routeInfo.params.subscribe((params:Params)=>{
		this.productId = params["id"]
		})

}

