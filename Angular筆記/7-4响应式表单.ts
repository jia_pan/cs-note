//https://www.youtube.com/watch?v=mppYpAhgjY0&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=50


FormControl

username:FormControl = new FormControl('aaa');

FormGroup
//可以代表一個部分，以下用時間起迄範例

formModel:FormGroup = new FormGroup({
	form:new FormControl(),
	to:new FormControl()
})

FormArray
//類似FormGroup，但她可以增加的
//以實際狀況來說，可能用戶需要添加email，可以添加多個



emails:FormArray = new FormArray([
	new FormControl('QQ@gmail.com'),
	new FormControl('CC@gmail.com'),
	])

//響應式表單使用另外的指令，並非模板式表單，來自於ReactFormModule

	類名					<這列都要用屬性綁定語法[]>							指令

FormGroup					formGroup									formGroupName


FormControl 			formControl								formControlName


FormArray																		formArrayName


1.表單若是使用form 開頭的，就是響應式表單
	表單若是使用ng   開頭的，就是使用模板式表單


2.表單式只能在模板中操作資料模型，
	響應式只能在代碼中操作資料模型


3.Name結尾的指令只能在[formGroup] 所包含的作用域內


	<input [formControl]="username1">
	<form [formGroup]="formModel" (submit)="submitHandel()">
		<input formControlName="username2">
		<div formGroupName="dateRange">
			起始日期:<input type="date" formControlName="form"></input>
			截止日期:<input type="date" formControlName="to"></input>
		</div>
		<div>
			<ul formArrayName="emails">
				<li *ngFor="let e of this.formModel.get('emails').controls;let i =index">
					<input type="email" [formControlName]="i">
				</li>	
			</ul>
		</div>
		<button type="submit"></button>
</form>

//ts


formModel:FormGroup = new FormGroup({
	dateRange:new FormGroup({
		form:new FormControl(),
		to:new FormControl()
	}),
	emails:new FormArray([
		new FormControl('QQ@gmail.com'),
		new FormControl('CC@gmail.com'),
	])
})
username:FormControl = new FormControl('aaa');


constructor(){}
submitHandel(value:any){
	console.log(this.formModel.value)
}



