//https://www.youtube.com/watch?v=jHuBCab2CVE&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=43


///一、在父組件調用子組件方法

//子組件
export class ChildComponent{



	@Output();
	emitData:EmitEmitter<stirng>= new EmitEmitter();//EmitEmitter 要用angular/ core 的
													//泛型<>，裡面包的就是你要emit出去的資料類型


	childLog(value){

		consoole.log(‵‵`子組件說${value}`)
	}

}
//Father

FatherComponent.html

<app-child #child1></app-child>
<app-child  #child1></app-child>


FatherComponent.ts

//1.在父組件ts中調用子組件方法
export class FaterComponent{


	@ViewChild(child1)
	child:ChildComponent
	


	callChildLog(value){

		this.cild.childLog('哈哈');//調用子組件方法
	}

}

//1.在父組件模板中調用子組件方法
<app-child #child1></app-child>
<app-child  #child2></app-child>
<button (click)="child2.childLog('哇哇')"/>






///二、ngAfterViewInit與ngAfterViewChecked


//初始化順序

子組件ngAfterViewInit
子組件ngAfterViewChecked
父組件ngAfterViewInit
父組件ngAfterViewChecked

///有變換產生，變更檢測機制觸發


子組件ngAfterViewChecked
父組件ngAfterViewChecked


ngAfterViewInit與ngAfterViewChecked 是在組件被組好處發，因此寫

export class FaterComponent{

	message:string;

	ngAfterViewInit():void{

		this.message = "Hello";//這樣AN會拋出錯誤，真的要寫，塞入setTimeOut就行了
	}
	ngAfterViewChecked():void{

		this.message = "Hello";//這樣AN會拋出錯誤
	}
	


}