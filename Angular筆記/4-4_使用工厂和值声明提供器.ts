//本章學習，如何使用工廠提供器



providers:[ProductService,LoggerServer]
//這段Array[0] 代表當組件聲明需要ProductService這種token時，實例化一個ProductService 類別給他使用


//當有一天，你需要根據參數使用不同的類別
//或是你需要給定參數，實例化類別，你就能使用工廠方法實例化參數
//這時可使用工廠提供器


//app-module.ts



providers:[{provide:ProductService,useFactory:()
	=>{
			let _logger = new LoggerServer();
			if(dev){

				return new ProductServer(_logger);
			}
			return new AnotherProductServer(_logger);


	})},LoggerServer]
//這段程式碼有兩個問題

1.LoggerServer與工廠方法緊密耦合



providers:[{provide:ProductService,useFactory:(logger:LoggerServer)
	=>{
			let _logger = logger;
			if(dev){

				return new ProductServer(_logger);
			}
			return new AnotherProductServer(_logger);


	},
	deps:[LoggerServer]//使用deps屬性可以將服務注入近來
},LoggerServer]

//第二個問題
//可以透過字串方式，自己寫服務

providers:[{provide:ProductService,useFactory:(logger:LoggerServer,config)//變更處
	=>{
			let _logger = logger;
			if(config.XX){//變更處

				return new ProductServer(_logger);
			}
			return new AnotherProductServer(_logger);


	},
	deps:[LoggerServer,APP_CONFIG]//使用deps屬性可以將服務注入近來
},LoggerServer,{
	//變更處
		provide:"APP_CONFIG"//自己取
			useValue:{XX:false}//自己設定
}]


