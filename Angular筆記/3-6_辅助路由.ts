聲明輔助路由有三個步驟

聲明輔助路顯示區

<router-outlet></router-outlet>
<router-outlet name="aux"></router-outlet>///這東西就是輔助路由

{path:"xxx",component:XxxComponent,outlet:"aux"}
{path:"yyy",component:YyyComponent,outlet:"aux"}



<a [routerLink]="["/home",{outlets:{aux:'xxx',primary:'home'}}]">Xxx</a>//primary 表示主插座會跳到哪
<a [routerLink]="["/product",{outlets:{aux:'yyy'}}]">Yyy</a>
