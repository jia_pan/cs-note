//https://www.youtube.com/watch?v=vmZec1PErAk&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=49

模板式表單式透過'指令'創建數據模型
1.ngForm
2.ngModel
3.ngModelGroup




NgForm//用來代表整個表單，An會「自動」把所有的form html 標籤上加上NgForm，An接管表單，即使你沒有申明
			//並會接管本來的 submit 事件

1.隱式創建FormGroup的物件

2.也可以在form標籤以外的標籤使用，比如div

3.會自動把它包含的所有標有 NgModel 的值給自動添加進去

4.an 會自動把所有form 標籤添加 ngForm，不想的話，可以加上 ngNoForm//<form ngNoForm></form>

5.可以透過模板上聲明取得ngForm實體

6.可以透過 ngSubmit 接管 submit

<form #myForm="ngForm" (ngSubmit)="submitHandel(myForm.value)">
	<div>用戶名<input type="text"></input></div>
	<button type="submit"></button>
</form>

<div>{{myForm.value |json}}</div>

//
submitHandel(value:any){
	console.log(value)
}
-------------------------------------


NgModel

1.隱式創建FormControl的物件

2.在 ngForm中申名ngModel，是不用[(ngModel)]這樣的，直接寫 ngModel就好，但要寫name


<div>用戶名<input ngModel name="username" type="text"></input></div>

在form所產生的物件的值，key名就會是 name所定義的
{
	username:12345
}

3.ngModel也可以利用模板上聲明取得NgModel實體

<form #myForm="ngForm" (ngSubmit)="submitHandel(myForm.value)">
<div>用戶名<input #username="ngModel" ngModel name="username" type="text"></input></div>
	<button type="submit"></button>
</form>

<div>{{myForm.value |json}}</div>
<div> {{username.value}}</div>

-------------------------------------
NgModelGroup

代表表單的一部分，允許你將表單變成擁有層層結構的方法

1.隱式創建FormGroup的物件

<form #myForm="ngForm" (ngSubmit)="submitHandel(myForm.value)">
	<div ngModelGroup="userInfo">
		<div>用戶名<input #username="ngModel" ngModel name="username" type="text"></input></div>
		<div>密碼<input #username="ngModel" ngModel name="password" type="text"></input></div>

	</div>
	<button type="submit"></button>
</form>

<div>{{myForm.value |json}}</div>
<div> {{username.value}}</div>


myForm.value 輸出

{
	userInfo:{
		username:123
		password:456
	}
}