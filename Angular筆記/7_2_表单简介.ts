//https://www.youtube.com/watch?v=rDA6M5kConI&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=48


html表單提供功能

1.顯示表單

2.校正用戶輸入

3.提交表單數據

`
<form action="/regist" method="post">
	<div> 用戶名<input type="text" required pattern="[a-zA-Z0-9]+"></input></div>//用 required 可以檢查必填，pattern檢查規則
	<div></div>
	<div></div>
	<div></div>
	<button type="submit">送出</button>
</form>
`

HTML表單僅能處理簡單，複雜的需求不好應對



//Angular 表單API


1.不管是模板式還是響應式，都有一組對應的數據模型來儲存表單數據
模板式:數據模型是由an基於你組件模板中的指令'隱式'創建的
響應式:擬通過編碼明確創建數據模型然後將模板上的HTML元素與底層數據綁在一起



2.數據模型並不是一個任一的對象，他是一個由angular/forms模塊中的一些特定的類
如FormControl，FormGroup，FormArray組成等組成。在模板式表單中，你無法直接訪問到這些類


3.響應式表單不會替你生成html，模板你依然要自己寫

要用模板式，需要在appmodule裡引入 FormModule，
要用響應式，需要在appmodule裡引入 ReactiveFormModule