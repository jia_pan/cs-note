//https://www.youtube.com/watch?v=VE0JDiai5dY&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=51
//響應式表單

formModel:FormGroup;

constructor(){
	this.formModel = new FormGroup({
		username:new FormControl(),
		mobile:new FormControl(),
		passwordGroup:new FormGroup({
			password:new FormControl(),
			pconfirm:new FormControl(),
		})
	})

}

//使用form builder 能讓代碼更簡潔


constructor(fb:FormBuilder){
	this.formModel = fb.group({
		username:[''],//[0]式初始值，[1]是教驗方法，[2]是異步教驗方法
		mobile:[''],
		passwordGroup:fb.group({
			password:[''],
			pconfirm:[''],
		})
	})

}