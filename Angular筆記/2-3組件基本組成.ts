//https://www.youtube.com/watch?v=WwH-c_PLZwE&list=PLnzrgyM1SBsaErGITq0_5QjLmExWU8KQJ&index=5
//一、資料夾結構
初始化資料夾

e2e
src
polyfills.ts//an配置文件，讓an可以在舊環境中執行
angular.cli.json//angulr cli 配置文件
karma.confg.js//自動化測試配置文件
package.json//npm 安裝相關文件
tslint.json//tslint配置文件


組件=裝飾器+模板+控制器
	可選注入物件	輸入屬性@inputs(),提供器providers,生命週期鉤子
	可選輸出對象	輸出屬性@outputs,樣式表,動畫,生命週期鉤子



//二、組件的基本結構
//app.module.ts

@NgModule({
	declarations:[//聲明這個組件中有甚麼東西，只能有'組件'，'指令'與'管道'
		AppComponents
	],
	imports:[//這個模塊所依賴的其他模塊

	],
	providers:[],//模塊中所提供的'服務'
	bootstrap:[AppComponents]//申明了組件的祖組件是甚麼

})
export class AppModule{

}