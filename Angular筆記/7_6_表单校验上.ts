

formModel:FormGroup;

//單一值自訂教驗器
mobileValidator(control:FormControl):any{
	let myreg = /0-9/;
	let valid = myreg.test.control.value;
	return valid?null:{mobile:true}
}

//針對group自訂教驗器
equalVAlidator(group:FormGroup):any{
	let password:FormControl = group.get("password") as FormControl;
	let pconfirm:FormControl = group.get("pconfirm") as FormControl;
	let valid:boolen = (password.value===pconfirm.value)

	return valid ?null:{equal:true}
}

constructor(fb:FormBuilder){
	this.formModel = fb.group({
		username:['',[Validators.required,Validators.minLength()]],//[0]式初始值，[1]是教驗方法，[2]是異步教驗方法
		mobile:['',[this.mobileValidator]],
		passwordGroup:fb.group({
			password:[''],
			pconfirm:[''],
		},{validator:this.equalVAlidator})
	})

}


onSubmit(){
	let isValid:boolen = this.formModel.get("username").valid;
	//username教驗，有一個沒過，就會是false
	let erros:any = this.formModel.get("username").errors;
	//錯誤
	if(this.formModel.valid){
		//全部的輸入都是合法的
	}



}