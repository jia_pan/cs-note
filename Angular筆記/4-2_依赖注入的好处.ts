@NgModule({
		providers:[ProductService]
		//等價於
		providers:[{provide:ProductService,useClss:ProductService}]
		//這裡涉及到了an的token概念代表一個可被注入物件的類型，
		//意味著註冊著一個類行為ProductService，當一個模組聲明需要一個ProductService的token時
		//實例化一個ProductService並將注入到該模組
})