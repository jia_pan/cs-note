//https://www.youtube.com/watch?v=cGgt7C_YPHs&list=PL9nxfq1tlKKkfCNnYKoC2yvSy4jo1AsoD&index=30
5.1 单例模式 | 介绍 —— Javascript 设计模式系统讲解与应用
- 介紹

- 演示

- 場景

- 總結

------------------------------------

## 介紹
	-系統中唯一使用

	-一個類只有一個實例

------------------------------------


## 演示
	-購物車

	-登入框

```javascript


class SingleObj{
	login(){console.log('login....')}
}
//js 沒有private 屬性，因此只能用閉包實踐
SingleObj.getIntence = (function(){
	let intence;
     return function(){

	if(!intence){
		intence = new SingleObj();
	}
	return intence;
}
})()
let obj1 = SingleObj.getIntence();
let obj2 = SingleObj.getIntence();
console.log(obj1===obj2);//true



console.log('----測試錯誤用法---')
console.log('不能用new 方法')

let obj3 = new SingleObj();
console.log(obj1===obj3);//false



```




## 場景

- JQ的$

- vue 的 vueX


## 總結
- 設計原則驗證

	- 符合單一職責原則，指實例化唯一的物件

	- 沒法具體開放封閉原則，但絕對不違反開放封閉原則