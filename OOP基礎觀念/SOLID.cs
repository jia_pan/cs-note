SOLID






S  SRP Single Reposibility Principle 單一責任原則

//定義
A class should have only one reason to change
一個類別應該只有一個改變的理由



//一句話記憶:把一個複雜的class 拆成多個class

一、何謂責任(Reposibility)
*責任 = reson to changer(改變的理由)

當一個類別擁有多個責任，代表負責多個不同工作，當需求變更時，變更類別的理由也不只一個

二、SRP精神
一個類別負擔太多責任，意味著該類別可以被切割
	*透過定義一個全新的類別
	*並對原類別進行適度的分割

SRP主要精神就是"提高內聚力"，意味著你可以想到一個清楚的理由去改變它

三、使用時機
1.兩個責任在不同時間點產生需求變更
ex:你想修改SQL語法，你去改Deal class
	 你想修改系統紀錄邏輯，你也要改Deal class
	 這時候，Deal class 既需要被拆分

2.類別中有一段程式碼，其他類別也用的到，那麼這段程式碼就需要杯拆分

3.系統中有個非必要功能(未來需求)，老闆又逼你做

四、SRP可能造成
類別變多，導致耦合力增加


五、SRP使用注意事項
參考YAGNI(You Ain't Gonna Need it)原則
	*不用急於第一時間專注分離原則
	*尚未出現的需求，不須預先分離責任
	*需求變更時，再進行類別分割


SRP是SOLID中最簡單，卻最難做到
*需要不斷提升開發經驗與重構技術
*若沒有足夠經驗去定義一個物件的責任，那麼建議不要過早進行SRP規劃


-----------------------------------------------

O OCP Open Close Principle 開放封閉原則



//定義 軟體實體(類別、模組、函式)應能開放擴充但封閉修改

//一句話記憶:你他媽新增就好，不要給我改
//一句話怎麼做:想辦法用Interface，再用多個模組去實作他


藉由增加新的程式碼，而不是修改原本已經存在的程式碼來擴充系統

一、OCP 的基本精神

*一個類別需要開放，就意謂該類別可以被擴充
	*可透過繼承
	*C#還有擴充方法

*一個類別需要封閉，意味著有其他人正在使用這個類別
	*如果程式已經編譯，但又已經有人在使用原本類別
	*封閉修改可以有效避免未知的問題發生


二、OCP實作方式

採用分離相依技巧(相依於抽象)
缺點:要進行重構


A--->B  //A 使用 B


A--->I<|---類別B

用I 抽象型別隔離

三、使用時機
*你既有類別已經清楚被定義，處於一個強調穩定的狀態

*你需要擴充現有類別，加入新需求或新的方法

*你擔心修改現在的程式碼會破壞現有系統運作




-----------------------------------------------
 三、L LSP Liskov Substitution Principle 李氏替換原則

 //定義
 //子型別必須可以汰換為他的基底型別
 //一句話:Interface 好啊

 若你的程式有採用繼承或介面，然後建立幾個不同的衍生型別。
 在你的系統中只要是基底型別出現的地方，都可以用子型別替代，
 且不會破壞系統原有的行為。


 一、關於LSP精神

當時做繼承時，比須確保型別轉換後還能得到正確的結果
*每個衍生類別都可以正確低替換為基底類別，且程式碼在執行時不會有異常
*必須正確的實作繼承與多型
//記得像是策略模式中的 洗澡鴨鴨不能有fly方法，解決方式



二、關於LSP實作方式
*採用類別繼承方式開發
	*須注意繼承的實作方式

*採用合約設機方式進行開發
	*利用介面來定義基底類型



-----------------------------------------------
I ISP Interface Segregation Principle 介面隔離原則
//定義
//A:多個'用戶端專用的介面'優於一個通用需求介面
//B:用戶端不應該強迫相依於沒有用到的介面


一、ISP的基本精神

*把不同需求的屬性與方法，放到不同介面當中
	*不要讓你的Interface包山包海
	*特定需求沒有用到的方法，不要加入到介面當中，另外建一個
	*可以拿interface當作群組來用
*使得系統可達到鬆散耦合、安全重構、功能擴充


二、ISP 實作方式
依據用戶端需求，將介面進行分割或群組

類別的使用時機可以被分割的時候
	*假設類別有20個方法，並實作一個有15個方法的介面
	*有某個用戶端只會使用該類別的10個方法
	*就可將這類別的十個方法定義介面並實作介面
	*用戶端改用介面操作
	*過程也能降低主程式與這個類別的耦合

三、範例

public void Main(){
	Driver o = new Driver();
	o.StratEngine();
	o.Drive();
	o.StopEngine();
}

public Interface IAllInOneCar
{
	void StratEngine();
	void Drve();
	void StopEngine();
	void ChangeEngine();
}

public class Driver:IAllInOneCar{
	public void StratEngine(){};
	public void Drve(){};
	public void StopEngine(){};
	public void ChangeEngine(){};//Main 客戶端沒有用到這個介面，應當拆出來
}

///修改後------------------------------------------------------
public void Main(){
	Driver o = new Driver();
	o.StratEngine();
	o.Drive();
	o.StopEngine();
}

public Interface IDriver
{
	public void StratEngine();
	public void Drve();
	public void StopEngine();
}

public class Driver:IAllInOneCar{
	public void StratEngine();
	public void Drve();
	public void StopEngine();
}

public Interface IMachinic
{
	void ChangeEngine();
}
public class Machinic:IMachinic
{
	public void ChangeEngine(){
		throw new NotImplentedException();
	}
}


-----------------------------------------------

D DIP Dependency Inversion Principle 相依反轉原則

*高階模組不該依賴於低節模組，兩者都要依賴於抽象

*抽象不應該依賴細節，而細節則應該相依抽象



一、DIP的基本精神
所有類別都要相依抽象，而非具體實作
	*可透過DI Container 

為了達到類別間鬆散耦合的目的
	*開發過程，所以類別的偶合關係一律透過抽象介面


二、相依反轉

Clinent
	||
	||
	ｖ
Service

public class Clinet{
	public Service _service = new Service();
	public void doSomething(){`
		_service.do();
	}

}

//進行反轉

Clinent
	||
	||
	ｖ
IService
	Ａ
	||
	||
	||
MyService 

public class Clinet{
	public IService _service ;
	public void Clinet(IService  service){
		_service=service
	}
	public void doSomething(){
		_service.do();
	}

}
public Interface IService{}
public class MyService:IService{}
public class YourService:IService{}
public class TheirService:IService{}

三、關於DIP實作
*別全部都相依於抽象，而非具體實作

*經過DIP後，原來相依於類別的程式碼
	*都改成相依於抽象類別
	*從緊密偶和變成鬆散耦合
	*依據需求，隨時抽換具體實作類別



四、DIP使用時機


*想降低偶何時

*希望類別都相依於抽象，團隊可以更有效率的開發系統

*想可以替換具體實作，讓系統變得更加有彈性
	*符合DIP通常也意外符合OCP與LSP原則
	*只要考慮SRRP與ISP就很棒了

*想導入TDD或單元測時時