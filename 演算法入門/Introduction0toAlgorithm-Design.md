## 20.Linear Search
>> 點點兵找法，一個一個找

```ts
function linearSearch(array:number[],target:number):number{
    for(let i=0 ;i < array.length;i++){
        if(array[i]===target) return i
    }
    return -1
}

```
### Overriew of Linear Search
worst Case Performance:O(n)
best Case Performance :O(1)
average performance O(n/2)

## 21.Binary Search
>> 排序完，兩堆兩堆分組找

```ts
function binarySearch(array:number[],target:number):number{
    let l = 0
    let r = array.length-1
    array.sort((a,b)=>a-b)
    while(l<=r){
        const mid = (l+r)>>1
        if(array[mid]===target){
             return  mid
        }
        else if(array[mid]<target){
            r= mid - 1
        }else{
            l = mid+1
        }
    }
    return -1
}

```
### Overriew of Linear Search
worst Case Performance:O(log n)
best Case Performance :O(1)
average performance O(log n)

## 22.Intersectino Probles
找兩個array 交集元素
```ts
function interseciton(n:number[];m:number[]):number[]{
    const result=[]
    for(let i = 0; i<n.length;i++){
        for(let j =0 ;j<m.length;j++){
            if(n[i]===m[j]) result.push(n[i])
        }
    }
    return result
}
```
worst Case Performance:O(n²)

## 24.counter
better 
使用counter(或叫計數版)
```ts
function interseciton(n:number[];m:number[]):number[]{
    const result = new Set()
    const counter:{[string]:number}={}
    const array = [...n,...m]
    for(let i = 0; i<array.length;i++){
        if(counter[array[i]]){
           result.add(array[i])
        }else{
            counter[array[i]=1
        }
    }
    return result
}
```
worst Case Performance:O(n)

## 25.Coding Pratice-Frenquency Problem
Write a function that takes two singles and chek if they have ths same letter.Order dones't matter.
Ex
smaeFrquency("abbc","aabc") false
sameFrquency("abba","abab") true
sameFrquency("aasdebasdf","adfeebed")fale

```ts
function samFrquecy(a:stirng,b:string):boolean{
    const aSList = a.split('').sort((a,b)=>a-b)
    const bSList = a.split('').sort((a,b)=>a-b)
    for(let i=0 ;i<aSlist.length;i++){
        if(!aSList[i]||!bSlit[i]) return false
        if(aSList[i]!===bSList[i]) return false
    }
    return true

}

```
## 26 Codeing Pratice-Average Pair

Wriete a function that geive a sorted array of integers and a number.Find if there's any pair in the array that has average of the given nunber.Find all of them.There might be multiple paris fit the condition.

Ex.
averagePair([-11,0,1,2,3,9,14,17,21],1.5) 3
The number pair is -11 and 14,0 and 3,1,and 2

```ts
function averagePair(arr:number[],avg:number):number{
    let result =[]
    for(let i =0 ;i<arr.length-1;i++){
       for(let j = i+1;j<arr.length,j++){
        if((arr[i]+arr[j]/2)===avg) {
            result.push([arr[i],arr[j]])
        }
       }
    }

}

```
worst Case Performance:O(n²)

## 27.Pointer
This is a general skill when doing algorithm design.Pointer is not a formal name.
Name is deffcrent, but th idea is ths same ecerywhere.
-It helps reduc the complexity of algorithms


```ts
function averagePair(arr:number[],avg:number):number{
    let result =[]
    let l =0
    let r = arr.length-1
    while(l<r){
        if((arr[l]+arr[r])>>1===avg){
            result.push([arr[l],arr[r]])
            l++
            r--
        }else if(arr[l]+arr[r])<avg){
            l++
        }else{
            r--
        }
    }
    return restul
}

```
worst Case Performance:O(n)

## 29 Subsequence Problem
A subsequence of a string is a new string that is formed from original stirng by deleting some(can be once)of the characters without disturbing the relative positions of the remaining charcvters.
Write a function that checks if one string is a subsequence of the other string

Ex:
isSubsequence('hello','hello Dear')//true
isSubsequence('book','booklyn')//true
isSubsequence('abc','bca')//false

```ts

function isSubsequence(string1:string,string2:string){
    const  pointer1=0
    const  pointer2=0
    while(pointer2<string2.length){
        if(string2[pointer2]===string1[pointer1]){
            pointer1++
        }
        if(pointer1>=string1.length-1){
            return true
        }
        pointer2++
    }
    return false
}
```

## Sliding Window
-This is a well-know algoriths
-Generally speaking,a slding window is a sub-list that runs over an underlying collection.
For example,if you have an arry like:
[a,b,c,d,e,f,g.,h]
Then sliding window of size 3 would run over it like
``` 
[a,b,c]
  [b,c,d]
    [c,d,e]
      [d,e,f]
```
### Codeing Pratice-Max and Min Sum
Write two function that calculate the max and min sum of n consecutive numbers in an array.
Ex.
maxSum([2,7,3,0,6,1,-5,-12,-11],3)//12
minSum([2,7,3,0,6,1,-5,-12,-11],3)//-28
```ts 

function maxSum(array:number[],size:number):number{
    let max_number = -Infinity
    if(size<array) return null
    for(let i = 0 ; i<= array.length-size;i++){
        let tem=0
        for(let j =i ;j<i+size;j++){
            tem+=array[j]
        }
        max_number = tem>max_number?tem:max_number
    }
    return max_number
}
```